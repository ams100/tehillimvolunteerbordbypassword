//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TehillimVulonteerBord.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tormim
    {
        public int num_torem { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string NameSpouse { get; set; }
        public string toar { get; set; }
        public string siomet { get; set; }
        public string street { get; set; }
        public string Street2 { get; set; }
        public string house { get; set; }
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string mikud { get; set; }
        public string country { get; set; }
        public string telh { get; set; }
        public string telw { get; set; }
        public string tel3 { get; set; }
        public string Fax1 { get; set; }
        public string Fax2 { get; set; }
        public string WebUser { get; set; }
        public string WebPwd { get; set; }
        public string mail { get; set; }
    }
}
