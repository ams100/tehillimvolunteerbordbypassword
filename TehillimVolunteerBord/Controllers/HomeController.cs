﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

using TehillimVulonteerBord.Models;

namespace TehillimVolunteerBord.Controllers
{
    public class HomeController : Controller
    {
        tehillimEntities db = new tehillimEntities();

        //[Authorize]
        public ActionResult Index(int? id)
        {
            if (Session["Validate"] == null || id == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.footer = db.EruimSeting.First(p => p.NameSet == "EndPrintText").ValSet;
            Session["VolunteerId"] = id;
            var torem = db.tormim.FirstOrDefault(p => p.num_torem == id);
            if (torem != null)
            {

                ViewBag.totals = 0;
                ViewBag.finish = 0;
                ViewBag.newRow = 0;
                var er = db.EruimRashi.Where(p => p.NumToremEr == id);
                if (er != null)
                {
                    ViewBag.totals = er.Count();
                    ViewBag.finish = er.Where(p => p.StatusErua == "סיים").Count();
                    ViewBag.newRow = er.Where(p => p.StatusErua == "חדש").Count();
                }
                return View(torem);
            }
            return View();
        }

        public JsonResult AjaxList()
        {
            if (Session["VolunteerId"] != null)
            {
                var id = (int)Session["VolunteerId"];
                var people = db.EruimRashi.Where(p => p.NumToremEr == id && p.DateSendFile == null).ToList();

                return Json(people, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult StatusList()
        {
            if (Session["VolunteerId"] != null)
            {
                var id = (int)Session["VolunteerId"];
                var dates = db.EruimRashi.Where(p => p.NumToremEr == id).Select(p => p.StatusErua).Distinct().ToList();
                var list = new List<dynamic>();
                foreach (var date in dates)
                {
                    list.Add(new { StatusErua = date });
                }
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LastNameList()
        {
            if (Session["VolunteerId"] != null)
            {
                var id = (int)Session["VolunteerId"];
                var dates = db.EruimRashi.Where(p => p.NumToremEr == id).Select(p => p.LastNameEr).Distinct().ToList();
                var list = new List<dynamic>();
                foreach (var date in dates)
                {
                    list.Add(new { LastNameEr = date });
                }
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FileList()
        {
            if (Session["VolunteerId"] != null)
            {
                var id = (int)Session["VolunteerId"];
                var dates = db.EruimRashi.Where(p => p.NumToremEr == id).Select(p => p.NameFileEr).Distinct().ToList();
                var list = new List<dynamic>();
                foreach (var date in dates)
                {
                    list.Add(new { NameFileEr = date });
                }
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult NextDateList()
        {
            if (Session["VolunteerId"] != null)
            {
                var id = (int)Session["VolunteerId"];
                var dates = db.EruimRashi.Where(p => p.NumToremEr == id).Select(p => p.DateNextKesher).Distinct().ToList();
                var list = new List<dynamic>();
                foreach (var date in dates)
                {
                    list.Add(new { DateNextKesher = date });
                }
                return Json(list, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(UpdateClass up)
        {
            try
            {
                var er = db.EruimRashi.FirstOrDefault(p => p.numerua == up.id);
                if (er != null)
                {
                    er.tguvaEr = up.tguvaEr;
                    er.DateLastKesher = DateTime.Now;
                    if (up.DateRegSiha != null) er.DateRegSiha = DateTime.ParseExact(up.DateRegSiha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    er.StatusErua = up.StatusErua;
                    if (er.StatusErua == "מעקב")
                    {
                        er.DateNextKesher = DateTime.ParseExact(up.DateNextKesher, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    db.SaveChanges();
                    try
                    {
                        if (up.sendToMenager != null && up.sendToMenager.Value)
                        {

                            MailMessage mail = new MailMessage();
                            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                            mail.From = new MailAddress("tehilim@gmail.com");



                            var mailAddress = db.EruimSeting.First(p => p.NameSet == "ManagerReminderEmail").ValSet.Split(',');
                            for (var i = 0; i < mailAddress.Length; i++)
                            {
                                mail.To.Add(mailAddress[i]);
                            }

                            mail.Subject = db.EruimSeting.First(p => p.NameSet == "ManagerMailSubjectOneRecord").ValSet;
                            var torem = db.tormim.First(p => p.num_torem == er.NumToremEr);
                            mail.Body = "מ" + torem.first_name + " " + torem.last_name + " " + torem.num_torem + System.Environment.NewLine +
                                "תאור: " + er.Teur + System.Environment.NewLine +
                                   "תגובה: " + er.tguvaEr + System.Environment.NewLine +
                                    "שם: " + er.NumToremOtherEr + ' ' + er.FirstNameEr + ' ' + er.LastNameEr;
                            SmtpServer.Send(mail);                         
                           
                            return Json(new { success = true, message = "המייל נשלח בהצלחה" }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    catch(Exception e)
                    {
                        return Json(new { success = false, message ="הייתה בעיה בשליחה: "+ e.Message }, JsonRequestBehavior.AllowGet);
                    }

                    

                }
            }
            catch(Exception e)
            {
                return Json(new { success = false, message = "היתה בעיה בעדכון " + e.Message}, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SendMailMonth(string[] NameFiles)
        {
            var message = "";
            try
            {
                var id = (int)Session["VolunteerId"];
               
                if (NameFiles == null)
                {
                    var eruims = db.EruimRashi.Where(p => p.NumToremEr == id);
                    NameFiles = new string[] { eruims.OrderByDescending(p => p.NameFileEr).First().NameFileEr };

                }


                //message = SendMailMonthMailToUser(id) + "***********************" + SendMailMonthMailToManager(id, NameFiles);
                var ifSand = SendMailMonthMailToUser(id) == "true" && SendMailMonthMailToManager(id, NameFiles) == "true";
               message = ifSand ? "הדו''ח נשלח בהצלחה!" : "שליחת הד''ח לא הצליחה עקב שגיאה. בדוק מול מנהל האתר שדוא''ל רשום במערכת";
               if (ifSand)
               {
                   for (var i = 0; i < NameFiles.Length; i++)
                   {
                       var name = NameFiles[i];
                       var eruims = db.EruimRashi.Where(p => p.NumToremEr == id && p.NameFileEr == name);
                       foreach (var e in eruims)
                       {
                           e.DateSendFile = DateTime.Now;
                       }
                   }
                   db.SaveChanges();
               }

            }
            catch (Exception e)
            {
                message = e.Message +" "+ e.InnerException;
             //   message = "שליחת הד''ח לא הצליחה עקב שגיאה. בדוק מול מנהל האתר שדוא''ל רשום במערכת";
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        public string SendMailMonthMailToUser(int id)
        {
            string retVal = "true";

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("tehilim@gmail.com");
                var torm = db.tormim.FirstOrDefault(p => p.num_torem == id);
                if (torm != null)
                {
                    string to = torm.mail;
                    //mail.To.Add("avigail.st@gmail.com");
                    mail.To.Add(to);
                    var mailtext = db.EruimSeting.First(p => p.NameSet == "UserMonthMail");
                    mail.Subject = mailtext.ValSet;
                    mail.Body = mailtext.ValSetB;

                    SmtpServer.Send(mail);
                    //retVal = "הדו''ח נשלח בהצלחה!";
                }
            }
            catch (Exception e)
            {
                retVal = e.Message;
                //retVal = "שליחת הד''ח לא הצליחה עקב שגיאה. בדוק מול מנהל האתר שדוא''ל רשום במערכת.";
            }
            return retVal;

        }
        public string SendMailMonthMailToManager(int id, string[] NameFiles)
        {
            string retVal = "true";
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("tehilim@gmail.com");

              
                var body= " הקבצים: " + Environment.NewLine;
                for(var i=0; i<NameFiles.Length; i++)
                {
                    var name =  NameFiles[i];
                    var eruim = db.EruimRashi.FirstOrDefault(p => p.NameFileEr == name && p.NumToremEr == id);
                    if (eruim != null)
                    {
                        body += eruim.NumFileEr.ToString() + ": " + name + Environment.NewLine;
                    }
                }
                var user = db.tormim.First(p => p.num_torem == id);
                var mailAddress = db.EruimSeting.First(p => p.NameSet == "ManagerReportEmail").ValSet.Split(',');
                for (var i = 0; i < mailAddress.Length; i++)
                {
                    mail.To.Add(mailAddress[i]);
                }

                mail.Subject = db.EruimSeting.First(p => p.NameSet == "ManagerMailSubjectMonthReport").ValSet + " " + user.first_name + " " + user.last_name + " " + user.num_torem;
                mail.Body = db.EruimSeting.First(p => p.NameSet == "ManagerMailBody").ValSet + body;
                SmtpServer.Send(mail);

            }
            catch (Exception e)
            {
                retVal = e.Message;
            }
            return retVal;
        }

        public JsonResult GetPrintData(int id)
        {
            var torem = db.tormim.FirstOrDefault(p => p.num_torem == id);
          
            if(torem != null)
            {
                
                var rows = db.EruimRashi.Where(p => p.NumToremEr == id && p.DateSendFile == null).ToList();
                var header = "<div id='header'>" + rows.First().NameFileEr + " " + torem.first_name + " " + torem.last_name + "</div>" ;
                var footer = db.EruimSeting.First(p => p.NameSet == "EndPrintText").ValSet;
                var body = "";
                foreach(var row in rows)
                {
                    body += "<p style='font-weight:bold'><span class='space'>" + row.Eara + " </span><span class='space'>" + row.FirstNameEr + " " + row.LastNameEr + "</span><span class='space'> " + row.TelEr + " " + row.PelEr + "</space></p>" +
                        "<p>" + row.Teur+ "</p>" + 
                          "<p>תגובה:"+ row.tguvaEr+ "</p>" +
                          "<p>תאריך:_________________</p><hr/>";
                  
                }
                body += "<div id='footer'>" + footer + "</div>";
                return Json(header + body, JsonRequestBehavior.AllowGet);

            }

            return Json("", JsonRequestBehavior.AllowGet);
       
        }

        public ActionResult GetPrintPage(int id)
        {
            var torem = db.tormim.FirstOrDefault(p => p.num_torem == id);
            ViewBag.Title = "דף להדפסה";
            if (torem != null)
            {
                ViewBag.ToremName = torem.last_name + " " + torem.first_name + " " + torem.WebUser;
                ViewBag.footer = db.EruimSeting.First(p => p.NameSet == "EndPrintText").ValSet;
                var rows = db.EruimRashi.Where(p => p.NumToremEr == id && p.DateSendFile == null).ToList();
                ViewBag.NameFileEr = rows.Count() > 0 ?  rows.First().NameFileEr : "";
             return View(rows);  

            }

            return View();

        }
    }

}