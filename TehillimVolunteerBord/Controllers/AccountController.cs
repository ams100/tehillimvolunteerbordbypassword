﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using TehillimVulonteerBord.Models;

namespace TehillimVulonteerBord.Controllers
{
    public class AccountController : Controller
    {
        tehillimEntities db = new tehillimEntities();
        public ActionResult Login()
        {
            return View();
        }

        public JsonResult LoginUser(string userName, string password)
        {
            var menagerName = db.EruimSeting.First(p => p.NameSet == "ManagerName").ValSet;
             var menagerPass = db.EruimSeting.First(p => p.NameSet == "ManagerPassword").ValSet;
            if(userName == menagerName && password == menagerPass)
            {
                return Json(new { isValide = true, isMenager = true }, JsonRequestBehavior.AllowGet);
            }
            var torem = db.tormim.FirstOrDefault(p => p.WebPwd == password && p.WebUser == userName);
            if(torem != null)
            {
                Session["Validate"] = true;
                db.EruimLog.Add(new EruimLog() { DateLog = DateTime.Now.Date, TimeLog = DateTime.Now.TimeOfDay, NumToremLog = torem.num_torem });
                db.SaveChanges();
                return Json(new { isValide = true, id = torem.num_torem, isMenager = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { isValide = false, isMenager = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PasswordForgot()
        {
            return View();
        }

        public JsonResult SendUserMaill(string email)
        {
            var torem = db.tormim.FirstOrDefault(p => p.mail == email);

            if (torem != null)
            {
                try
                {
                    MailMessage mail = new MailMessage(); SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("myemail@gmail.com");
                    //mail.To.Add("avigail.st@gmail.com");
                   mail.To.Add(email);
                    mail.Subject = "סיסמת כניסה לאתר תהילים";
                    mail.Body = "פירטי הגישה:\nשם משתמש:" + torem.WebUser + "\n" + "סיסמא:" + torem.WebPwd;

                    SmtpServer.Send(mail);
                    return Json(torem.num_torem, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
	}
}