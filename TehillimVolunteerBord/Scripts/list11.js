﻿$(document).ready(function () {
    $("#deleteFilter").jqxButton({
        width: '150'
    });
    var isNew = false;
    function createSingleFieldDataAdapter(fieldName, fieldType, url) {
        var source = {
            dataType: "json",
            dataFields: [{
                name: fieldName,
                type: fieldType
            }
            ],
            url: url,
            id: fieldName
        };

        return new $.jqx.dataAdapter(source);
    }

    function dateRenderer(index, label, value) {
        return value == null ? "לא מוגדר" : value.toLocaleDateString();
    }

    function nullRenderer(index, label, value) {
        return value || "לא מוגדר";
    }

    $("#statusFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("StatusErua", "string", "/Home/StatusList"),
        displayMember: "StatusErua",
        valueMember: "StatusErua",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: nullRenderer,
        rtl: true
    });
    $("#nameFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("LastNameEr", "string", "/Home/LastNameList"),
        displayMember: "LastNameEr",
        valueMember: "LastNameEr",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: nullRenderer,
        rtl: true
    });
    $("#fileFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("NameFileEr", "string", "/Home/FileList"),
        displayMember: "NameFileEr",
        valueMember: "NameFileEr",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: nullRenderer,
        rtl: true
    });
    $("#DateFilter").jqxComboBox({
        source: createSingleFieldDataAdapter("DateNextKesher", "date", "/Home/NextDateList"),
        displayMember: "DateNextKesher",
        valueMember: "DateNextKesher",
        checkboxes: true,
        width: '200',
        height: '25',
        autoDropDownHeight: true,
        renderer: dateRenderer,
        rtl: true
    });

    $('#statusFilter').on('bindingComplete', function (event) {
        $('#statusFilter').jqxComboBox('checkAll');
        $('#statusFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#nameFilter').on('bindingComplete', function (event) {
        $('#nameFilter').jqxComboBox('checkAll');
        $('#nameFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#fileFilter').on('bindingComplete', function (event) {
        $('#fileFilter').jqxComboBox('checkAll');
        $('#fileFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#DateFilter').on('bindingComplete', function (event) {
        $('#DateFilter').jqxComboBox('checkAll');
        $('#DateFilter').data("bound", true);
        applyFiltersAndSetCheckChangeEvents();
    });

    $('#deleteFilter').on("click", function () {
        unsetCheckChangeEvents();
        $('#statusFilter').jqxComboBox('checkAll');
        $('#nameFilter').jqxComboBox('checkAll');
        $('#fileFilter').jqxComboBox('checkAll');
        $('#DateFilter').jqxComboBox('checkAll');
        applyFiltersAndSetCheckChangeEvents();
    });

    function updateFilterComboboxesBoundData() {
        $("#statusFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("StatusErua", "string", "/Home/StatusList")
        });
        $("#nameFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("LastNameEr", "string", "/Home/LastNameList")
        });
        $("#fileFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("NameFileEr", "string", "/Home/FileList")
        });
        $("#DateFilter").jqxComboBox({
            source: createSingleFieldDataAdapter("DateNextKesher", "date", "/Home/NextDateList")
        });
    }

    function applyFiltersAndSetCheckChangeEvents() {
        if ($('#statusFilter').data("bound") && $('#nameFilter').data("bound") && $('#fileFilter').data("bound") && $('#DateFilter').data("bound")) {
            applyFilter("StatusErua", "stringfilter", "statusFilter");
            applyFilter("LastNameEr", "stringfilter", "nameFilter");
            applyFilter("NameFileEr", "stringfilter", "fileFilter");
            applyFilter("DateNextKesher", "datefilter", "DateFilter");
            $('#statusFilter').on('checkChange', function (event) {
                applyFilter("StatusErua", "stringfilter", "statusFilter");
            });

            $('#nameFilter').on('checkChange', function (event) {
                applyFilter("LastNameEr", "stringfilter", "nameFilter");
            });

            $('#fileFilter').on('checkChange', function (event) {
                applyFilter("NameFileEr", "stringfilter", "fileFilter");
            });

            $('#DateFilter').on('checkChange', function (event) {
                applyFilter("DateNextKesher", "datefilter", "DateFilter");
            });
        }
    }

    function unsetCheckChangeEvents() {
        $('#statusFilter').off('checkChange', '**');

        $('#nameFilter').off('checkChange', '**');

        $('#fileFilter').off('checkChange', '**');

        $('#DateFilter').off('checkChange', '**');
    }

    function applyFilter(datafield, filtertype, filterbox) {
        // create a new group of filters.
        var filtergroup = new $.jqx.filter();
        // get listbox's checked items.
        var checkedItems = $("#" + filterbox).jqxComboBox('getCheckedItems');
        if (checkedItems.length == 0) {
            var filter_or_operator = 1;
            var filtervalue = "Empty";
            var filtercondition = 'equal';
            var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
            filtergroup.addfilter(filter_or_operator, filter);
        } else {
            for (var i = 0; i < checkedItems.length; i++) {
                var filter_or_operator = 1;
                // set filter's value.
                var filtervalue = checkedItems[i].label;
                // set filter's condition.
                var filtercondition = 'equal';
                // create new filter.
                var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
                // add the filter to the filter group.
                filtergroup.addfilter(filter_or_operator, filter);
            }
        }
        $("#dataTable").jqxDataTable('removeFilter', datafield);
        // add the filters.
        $("#dataTable").jqxDataTable('addFilter', datafield, filtergroup);
        // apply the filters.
        $("#dataTable").jqxDataTable('applyFilters');
    }

    function expandCollapseRows() {
        var method = $("#collapseAll").val() ? "hideDetails" : "showDetails";
        var rows = $("#dataTable").jqxDataTable("getRows");
        for (var i = 0; i < rows.length; i++) {
            $("#dataTable").jqxDataTable(method, i);
        }
    };

    $("#collapseAll").jqxCheckBox({
        width: 20,
        height: 25
    });
    $("#ifSendMeneger").jqxCheckBox({
        width: 40,
        height: 25
    });

    $("#collapseAll").on('change', function (event) {
        expandCollapseRows();
    });
    $("#notSendReportDialog").jqxWindow({
        resizable: false,

        width: 400,
        height: 200,
        autoOpen: false
    });
    $("#sendReportDialog").jqxWindow({
        resizable: false,

        width: 400,
        height: 200,
        autoOpen: false
    });

    $("#dialog").jqxWindow({
        resizable: false,

        width: 700,
        height: 500,
        autoOpen: false
    });

    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
        switch (value) {
            case "סיים":
                return '<span style=" color: green;">' + value + '</span>';
            case "חדש":
                return '<span style=" color: red;">' + value + '</span>';
            default:
                return '<span style="color: blue;">' + value + '</span>';
        }
    };

    var index = 0;
    var initRowDetails = function (id, row, element, rowinfo) {
        var details = null;
        var information = null;
        var notes = null;
        // update the details height.
        rowinfo.detailsHeight = 150;
        element.append($("<div class='information' tabindex=" + index + " id='" + id + "'></div>"));
        index++;
        information = $(element.children()[0]);

        if (information != null) {
            var container = $('<div style="margin: 5px;"></div>')
            container.appendTo($(information));
            var table1 = $('<table style="direction:rtl;width:100%" ></table>');
            var table2 = $('<table style="direction:rtl;width:100%"></table>');
            var table3 = $('<table style="direction:rtl;width:100%"></table>');
            container.append(table1);
            container.append(table2);
            container.append(table3);
            isNew = isNew || row.StatusErua == "חדש";
            var table1Content = '<tr ><td width="10%"  ><button class="edit">עריכה</button></td>' +
				'<td width="15%">' + (row.Eara || "") + '</td><td width="5%">' +
				'</td><td width="5%"></td><td width="20%">' +
				(row.LastNameEr || "") + " " + (row.FirstNameEr || "") +
				'</td><td width="20%" > </td> <td width="15%">' +
				(row.TelEr || "") + '</td><td width="20%" >' +
				(row.PelEr || "") + '</td></tr>';

            $(table1).append(table1Content);
            var table2Content = '<tr ><td width="40%">' + (row.Teur || "") + '</td><td width="30%">' + (row.Sivug || "") +
				'</td><td width="20%">קובץ:' + (row.NameFileEr || "") + '</td><td width="20%">תאריך ליצירת קשר:' +
				(row.DateNextKesherForDisplay || "") +
				'</td></tr>';
            $(table2).append(table2Content);
            var statusColor = row.StatusErua == "סיים" ? 'green' : row.StatusErua == "חדש" ? "red" : "blue";

            var table3Content = '<tr ><td width="35%">תגובה:' + (row.tguvaEr || "") + '</td><td width="45%"></td><td width="10%" style="color:' + statusColor + ' "> סטטוס:' + (row.StatusErua || "") + '</td>' +
				'<td width="20%"> תאריך שיחה אחרונה:' + (row.DateLastKesherForDisplay || "") + '</td> </tr>';

            $(table3).append(table3Content);

            $("#" + id + " button.edit").on('click', function () {
                // update the widgets inside jqxWindow.
                $("#dialog").jqxWindow('setTitle', "ערוך שורה: " + (row.LastNameEr || "") + " " + (row.FirstNameEr || ""));
                var x = ($(window).width() - $("#dialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
                var y = ($(window).height() - $("#dialog").jqxWindow('height')) / 2 + $(window).scrollTop();
                $("#dialog").jqxWindow({ position: { x: x, y: y } });
                $("#dialog").jqxWindow('open');
                $("#dialog").attr('data-row-id', id);
                $("#dataTable").jqxDataTable({
                    disabled: true
                });
                $("#Dname").val((row.LastNameEr || "") + " " + (row.FirstNameEr || ""));
                $("#DTeur").val(row.Teur || "");
                $("#DtguvaEr").val(row.tguvaEr || "");
                $("#DStatusErua").val(row.StatusErua || "");
                $("#dialog").css('visibility', 'visible');
            });
        }
    }

    // prepare the data
    var source = {
        dataType: "json",
        dataFields: [{
            name: 'Eara',
            type: 'string'
        }, {
            name: 'LastNameEr',
            type: 'string'
        }, {
            name: 'FirstNameEr',
            type: 'string'
        }, {
            name: 'TelEr',
            type: 'string'
        }, {
            name: 'Teur',
            type: 'string'
        }, {
            name: 'NameFileEr',
            type: 'string'
        }, {
            name: 'DateNextKesher',
            type: 'date'
        }, {
            name: 'tguvaEr',
            type: 'string'
        }, {
            name: 'StatusErua',
            type: 'string'
        }, {
            name: 'DateLastKesher',
            type: 'date'
        }
        ],
        url: "/Home/AjaxList/",
        id: 'numerua'
    };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $("#dataTable").jqxDataTable({
        width: 1190,
        rtl: true,
        source: dataAdapter,
        pageable: true,
        pageSize: 6,
        rowDetails: true,
        sortable: true,
        theme: 'ui-redmond',
        selectionMode: "singleRow",
        ready: function () {

            // called when the DataTable is loaded.
            // init jqxWindow widgets.
            $("#Dname").jqxInput({
                rtl: true,
                disabled: true,
                width: 150,
                height: 30
            });

            var ddSource = ["סיים", "מעקב", "חדש"];
            $("#DStatusErua").jqxDropDownList({
                source: ddSource,
                selectedIndex: 1,
                width: '200',
                height: '25',
                rtl: true
            });
            $("#DStatusErua").on('change', function (event) {
                var args = event.args;
                if (args) {

                    var item = args.item;

                    var value = item.value;
                    if (value == "מעקב") {
                        $("#divDateNextKesher").show();
                    } else {
                        $("#divDateNextKesher").hide();
                    }
                }
            });
            $("#DDateNextKesher").jqxDateTimeInput({
                rtl: true,
                formatString: 'd',
                width: 150,
                height: 30
            });

            $("#save").jqxButton({
                height: 30,
                width: 80
            });
            $("#cancel").jqxButton({
                height: 30,
                width: 80
            });
            $("#cancel").on("click", function () {
                // close jqxWindow.
                $("#dialog").jqxWindow('close');
            });

            $("#save").on("click", function () {
                //close jqxWindow.
                $("#dialog").jqxWindow('close');
                // update edited row.
                var rowId = parseInt($("#dialog").attr('data-row-id'));
                $.ajax({
                    type: "POST",
                    url: "/Home/Update/",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        id: rowId,
                        StatusErua: $("#DStatusErua").val(),
                        DateNextKesher: $("#DDateNextKesher").val(),
                        tguvaEr: $("#DtguvaEr").val()
                    }),
                    dataType: "json",
                    success: function (data, status, jqXHR) { }
                });
                var rowData = {
                    StatusErua: $("#DStatusErua").val(),
                    DateNextKesher: $("#DDateNextKesher").val(),
                    tguvaEr: $("#DtguvaEr").val()

                };

                setTimeout(function () {
                    $("#dataTable").jqxDataTable('updateBoundData');
                }, 1000);
                //updateFilterComboboxesBoundData();
            });
            $("#dialog").on('close', function () {
                // enable jqxDataTable.
                $("#dataTable").jqxDataTable({
                    disabled: false
                });
            });

        },
        pagerButtonsCount: 8,

        initRowDetails: initRowDetails,
        columns: [{
            text: '',
            dataField: 'Eara',
            width: 150,
        }, {
            text: 'שם משפחה',
            dataField: 'LastNameEr',
            width: 175,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'שם פרטי',
            dataField: 'FirstNameEr',
            width: 170,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'טלפון',
            dataField: 'TelEr',
            width: 200,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'קובץ',
            dataField: 'NameFileEr',
            width: 100,
            align: 'center',
            cellsalign: 'center'
        }, {
            text: 'תאריך ליצירת קשר',
            dataField: 'DateNextKesher',
            width: 150,
            align: 'center',
            cellsalign: 'center',
            cellsformat: 'd/M/yy'
        }, {
            text: 'סטטוס',
            dataField: 'StatusErua',
            width: 220,
            align: 'center',
            cellsalign: 'center',
            cellsrenderer: cellsrenderer
        }

        ]
    });

    var currentPage = 0,
	selectedDetails;

    function selectAppropriateRow() {
        if (!selectedDetails) return;
        var key = selectedDetails.attr("id");
        var row = $("#dataTable #contenttabledataTable tr[data-key=" + key + "]");
        var allRows = $("#dataTable #contenttabledataTable tr[role='row']");
        var index = allRows.index(row) + currentPage * $("#dataTable").jqxDataTable('pageSize');
        $("#dataTable").jqxDataTable("selectRow", index);
    }

    $('#dataTable').on('bindingComplete', function (event) {
        expandCollapseRows();
        bindInformationEvent();        
        selectAppropriateRow();
        $("#dataTable").jqxDataTable('goToPage', currentPage);
        if (selectedDetails) $("html").scrollTo(selectedDetails, 1000, { offset: { top: -180 } });
    });

    $('#dataTable').on('pageChanged', function (event) {
        currentPage = event.args.pagenum;
        setTimeout(function () {
            expandCollapseRows();
            bindInformationEvent();
        }, 0);
    });

    $('#dataTable').on('rowSelect', function (event) {
        var key = event.args.key;
        if (selectedDetails)
            selectedDetails.removeAttr("selectedDetails");
        selectedDetails = $("#dataTable div#" + key + ".information");
        selectedDetails.attr("selectedDetails", true);
    });

    function bindInformationEvent() {
        var infoDivs = $("#dataTable div.information");
        infoDivs.off("click", "**");
        infoDivs.on("click", function (event) {
            if (selectedDetails)
                selectedDetails.removeAttr("selectedDetails");
            selectedDetails = $(event.currentTarget);
            selectAppropriateRow();
            selectedDetails.attr("selectedDetails", true);
        });
    }

    $("#btnSendMonthReport").jqxButton();
    $("#print").jqxButton();
    $('#btnSendMonthReport').on('click', function () {
        if (isNew) {
            $("#notSendReportDialog").jqxWindow('open');
            var x = ($(window).width() - $("#notSendReportDialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
            var y = ($(window).height() - $("#notSendReportDialog").jqxWindow('height')) / 2 + $(window).scrollTop();
            $("#notSendReportDialog").jqxWindow({ position: { x: x, y: y } });
            $("#notSendReportDialog").css('visibility', 'visible');
            return;
        }
        else {
            $("#sendReportDialog").jqxWindow('open');
            var x = ($(window).width() - $("#sendReportDialog").jqxWindow('width')) / 2 + $(window).scrollLeft();
            var y = ($(window).height() - $("#sendReportDialog").jqxWindow('height')) / 2 + $(window).scrollTop();
            $("#sendReportDialog").jqxWindow({ position: { x: x, y: y } });
            $("#sendReportDialog").css('visibility', 'visible');
        }
    });

    $("#sendCancel").mousedown(function () {
        // close jqxWindow.
        $("#sendReportDialog").jqxWindow('close');
    });

    $("#send").mousedown(function () {
        //close jqxWindow.
        $("#sendReportDialog").jqxWindow('close');
        // update edited row.

        $.ajax({
            type: "GET",
            url: "/Home/SendMailMonth?NumFile=",
            contentType: "application/json; charset=utf-8",

            dataType: "json",
            success: function (data) {
                alert(data);
            }
        });
    });

    function GetNewStatus() {

        var rows = $("[data-status]");
        var statuses = new Array();
        var contains = false;

        rows.each(function () {
            contains = false;
            var item = $(this);
            if ($(item).data("status").toString() == 'חדש') {
                contains = true;
            }

        });
        if (contains == true) {
            alert('רשומות בסטטוס חדש לא ישלחו למנהל מערכת');
        }

    }

});
