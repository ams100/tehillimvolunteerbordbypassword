﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TehillimVulonteerBord.Controllers
{
    public class PdfUploadController : Controller
    {
        //
        // GET: /PdfUpload/
        public ActionResult Index(string message1, string message2 )
        {
            //if (!GlobalVar.IsManager)
            //{
            //    return RedirectToAction("Login", "login");
            //}
            ViewBag.Message1 = message1;
            ViewBag.Message2 = message2;
            return View();
        }

      
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase pdf1, HttpPostedFileBase pdf2)
        {
           
            string message1="" ;
            string message2="";
            //try
            //{
              
                if (pdf1 != null && pdf1.ContentLength > 0)
                {
                    var fileName = "pdf1.pdf";// Path.GetFileName(pdf.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/PDF/"), fileName);
                    pdf1.SaveAs(path);

                    message1 = "הקובץ ימים מיוחדים עלה בהצלחה ";
                }
            //}
            //catch
            //{
            //    message1 = "שגיאה! הקובץ ימים מיוחדים לא עלה, נסו שנית";
            //}
            //try{
                if (pdf2 != null && pdf2.ContentLength > 0)
                {
                    var fileName = "pdf2.pdf";// Path.GetFileName(pdf.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/PDF/"), fileName);
                    pdf2.SaveAs(path);
                    message1 = "הקובץ מסר החודש עלה בהצלחה ";
                }
            //}
            //catch (Exception e)
            //{
            //    message2 = "שגיאה! הקובץ מסר החודש לא עלה, נסו שנית";
            //}
            return RedirectToAction("Index", new {message1 = message1 ,message2 = message2 });
        }

      
	}
}